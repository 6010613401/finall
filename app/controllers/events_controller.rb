class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [ :edit, :update, :destroy,:new,:create ,:show]
  # GET /events
  # GET /events.json
  def index
    @events = Event.all.order("created_at DESC")
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @events = Event.all
  end

  # GET /events/new
  def new
    @event = current_register.events.new
  end

  # GET /events/1/edit
  def edit
    @event.register = current_register
  end

  # POST /events
  # POST /events.json
  def create
    @event = current_register.events.build(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end
  def search
    if params[:search].blank?  
      redirect_to(root_path, alert: "Empty field!") and return  
    else  
      @parameter = params[:search].downcase  
      @results = Event.all.where("lower(category) LIKE :search", search: @parameter)  
    end  
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  def vote
    @event = Event.find(params[:id])
    @event.upvote_by current_register
    redirect_back fallback_location: root_path
  end
    
  def downvote
    @event = Event.find(params[:id])
    @event.downvote_by current_register
    redirect_back fallback_location: root_path
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, :category, :description, :by ,:start, :end, :phone)
    end
    
   

end
