class ApplicationController < ActionController::Base
  helper_method :current_register
  helper_method :authenticate_user!
  def current_register
    begin
      if session[:register_id]
        @current_register ||= Register.find(session[:register_id])
      else
        @current_register = nil
      end
    rescue ActiveRecord::RecordNotFound => e
      puts e.message
      @current_register = nil
    end
  end
  
  def authenticate_user!
    begin
      current_register
      if current_register
      else
        redirect_to login_path,notice: 'You must be logged in frist.'
      end
    rescue ActiveRecord::RecordNotFound => e
      puts e.message
      @current_register = nil

    end
  end
end
