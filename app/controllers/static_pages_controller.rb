class StaticPagesController < ApplicationController
  def index
    
  end
  
  def homepage
    @events = Event.all
    @reports = Report.all
    
  end

  def help
  end
  
  def about
  end
  
  def howto
  end
  
  def contact
  end
  
  def static
  end
  
end
