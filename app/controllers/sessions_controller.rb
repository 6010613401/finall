class SessionsController < ApplicationController
  def new
  end
  def create
    register = Register.find_by_email(params[:email])
    if register && register.authenticate(params[:password])
      session[:register_id] = register.id
      redirect_to root_url, notice: "Logged in!"
    else
      flash.now[:alert] = "Email or password is invalid"
      render "new"
    end
  end
  
  def destroy
    session[:register_id] = nil
    redirect_to root_url, notice: "Logged out!"
  end
end

