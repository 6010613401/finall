class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.integer :event_id
      t.text :body
      t.references :register, null: false, foreign_key: true

      t.timestamps
    end
    add_index :comments, :event_id
  end
end
