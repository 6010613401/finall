Feature: Manage events
  In order to [goal]
  [stakeholder]
  wants [behaviour]
  
  Scenario: Register new events
    Given I am on the new events page 
    Then "Title" should not be disabled
    When I fill in "Title" with "title 1"
    When I select "food" from "Category"
    When I fill in "Description" with "description 1"
    And I fill in "By" with "by 1"
    And I fill in "Start" with "2019, Aug, 25"
    And I fill in "End" with "2019, Aug, 26"
    And I fill in "Phone" with "phone 1"
    Then I press "Save"
    
    
    Then I should see "title 1"
    And I should see "food"
    And I should see "description 1"
    And I should see "by 1"
    And I should see "2019-08-25 "
    And I should see "2019-08-26 "
    And I should see "phone 1"
