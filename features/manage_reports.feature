Feature: Manage reports
  In order to [goal]
  [stakeholder]
  wants [behaviour]

    

  Scenario: Delete report
    Given the following reports:
      |event|report_to|by|desciption|
      |event 1|report_to 1|by 1|desciption 1|
      |event 2|report_to 2|by 2|desciption 2|
      |event 3|report_to 3|by 3|desciption 3|
      |event 4|report_to 4|by 4|desciption 4|
    When I delete the 3rd report
    Then I should see the following reports:
      |Event|Report_to|By|Desciption||||
      |event 1|report_to 1|by 1|desciption 1|Show|Edit|Destroy|
      |event 2|report_to 2|by 2|desciption 2|Show|Edit|Destroy|
      |event 4|report_to 4|by 4|desciption 4|Show|Edit|Destroy|
