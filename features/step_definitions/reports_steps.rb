Given /^the following reports:$/ do |reports|
  Report.create!(reports.hashes)
end

When /^I fill in "(.*?)" date field with "(.*?)"$/ do |field_name, date_components|
  label = find("label", text: field_name)
  select_base_id = label[:for]
  date_components.split(",").each_with_index do |value, index|
    select value.strip, from: "#{select_base_id}_#{index}i"
  end
end

When /^I delete the (\d+)(?:st|nd|rd|th||||) report$/ do |pos|
  visit reports_path
  within("table tr:nth-child(#{pos.to_i})") do
    click_link "Destroy"
  end
end

Then /^I should see the following reports:$/ do |expected_reports_table|
  rows = find("table").all('tr')
  table = rows.map{ |r| r.all('th,td').map{|c| c.text.strip}}
  expected_reports_table.diff!(table)
end
